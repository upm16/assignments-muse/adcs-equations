import numpy as np
Array = np.ndarray


def f_euler_explicito(_t, _u, _f, _dt) -> None:
    return _u + _dt * _f(_u, _t)


def f_rk4(_t, _u, _f, _dt):
    k1 = _dt * _f(_u, _t)
    k2 = _dt * _f(_u + k1 / 2e0, _t + _dt / 2e0)
    k3 = _dt * _f(_u + k2 / 2e0, _t + _dt / 2e0)
    k4 = _dt * _f(_u + k3, _t + _dt)
    return _u + k1 / 6e0 + k2 / 3e0 + k3 / 3e0 + k4 / 6e0


def int_wrapper(
        ts: Array,
        us: Array,
        f,
        dt_base: float,
        method=f_euler_explicito) -> None:
    t = ts[0]
    us[1, :] = us[0, :]
    i = 1
    while True:
        if t + dt_base > ts[i]:
            dt = ts[i] - t
        else:
            dt = dt_base
            pass
        us[i, :] = method(t, us[i, :], f, dt)
        # us[i, :] = us[i, :] + dt * f(us[i, :], t)

        if t >= ts[-1]:
            break
            pass

        if t >= ts[i]:
            us[i + 1, :] = us[i, :]
            i = i + 1
            pass

        t = t + dt

        pass
    pass


def euler_explicito(*args, **kwargs):
    return int_wrapper(*args, **kwargs, method=f_euler_explicito)


def rk4(*args, **kwargs):
    return int_wrapper(*args, **kwargs, method=f_rk4)


__all__ = ["euler_explicito", "rk4"]
