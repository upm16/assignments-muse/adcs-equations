import numpy as np


def euler_freetorque_eq(inertia_matrix_b: np.ndarray, omega_b: np.ndarray) -> np.ndarray:
    a = inertia_matrix_b
    b = - np.cross(omega_b, np.matmul(inertia_matrix_b, omega_b))
    return np.linalg.solve(a, b)
    pass
