from math import exp, sqrt, pi
from ..constants import H, C, K, SIGMA, PHI, UA


def energia_proton(lamb: float) -> float:
    """Energia del proton en funcion de su longitud de onda.

    :param: lamb: Longitud de onda [m].
    """
    return H * C / lamb


def frecuencia_proton(lamb: float) -> float:
    """Frecuencia del proton en funcion de su longitud de onda.

    :param: lamb: Longitud de onda [m].
    """
    return C / lamb


def intensidad(temp: float, lamb: float) -> float:
    """Intensidad (por unidad de area y metro)
    :param: temp: Temperatura [K] del cuerpo.
    :param: lamb: Longitud de onda [m].
    """

    t1: float = 2e0 * H * C ** 2 / (lamb ** 5)
    t2: float = exp(H * C / (lamb * K * temp)) - 1e0

    return t1 / t2


def intensidad_blackbody(temp: float) -> float:
    """Intensidad (por unidad de area) de un cuerpo negro.
    :param: temp: Temperatura [K] del cuerpo.
    """

    return SIGMA * temp ** 4


def ley_wien(temp: float) -> float:
    """Longitud de onda [m] a la que la intensidad es máxima.
    :param: temp: Temperatura [K] del cuerpo.
    """

    return 2.898e-3 / temp


def distribucion_maxwellboltzman(m: float, temp: float, v: float) -> float:
    """Distribución de probabilidad de que exista
    una particula de masa 'm', temperatura 'temp' y velocidad 'v'.
    :param: m [kg] masa de la particula.
    :param: temp [K] temperatura de la particula.
    :param: v [m/s] velocidad de la particula.
    """

    r1: float = (m / (2e0 * pi * K * temp)) ** 3
    t1: float = sqrt(r1)
    t2: float = 4e0 * pi * v ** 2
    t3: float = exp(-m * v ** 2 / (2e0 * K * temp))
    return t1 * t2 * t3


def energia_solar(r: float) -> float:
    """Energia solar sobre un S/C a distancia r.

    :param: r: distancia [km] del Sol.
    """
    return PHI * (UA / r) ** 2


def presion_solar(phi: float, q: float) -> float:
    """Presion solar.

    :param: phi: flujo solar [W/m^2] del Sol.
    :param: q: reflectividad de la superficie.
    """

    return phi / C * (1e0 + q)


def aceleracion_solar(ps: float, a: float, m: float) -> float:
    """Presion solar.

    :param: ps: presion solar [Pa].
    :param: a: area [m^2] de la superficie.
    :param: m: masa [kg] del S/C..
    """

    return ps * a / m
