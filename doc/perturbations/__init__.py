from doc.constants import MU_E, R_E, cosd, sind
from numpy.polynomial.legendre import legval, legder
import numpy as np
import re
import os


N_MAX_DEF = 2
PC_N_MAX = 10


def _get_perturbation_coefs(filename: str) -> tuple:
    pattern = r"\s*(?P<N>[+\w.E-]+)" \
              r"\s+(?P<M>[+\w.E-]+)\s+" \
              r"(?P<CNM>[+\w.E-]+)\s+" \
              r"(?P<SNM>[+\w.E-]+).*"

    cnms = np.zeros((PC_N_MAX, PC_N_MAX))
    snms = np.zeros((PC_N_MAX, PC_N_MAX))
    jns = np.zeros(PC_N_MAX)

    with open(filename, 'r') as f:
        for line in f.readlines():
            match = re.search(pattern, line)
            n = int(match.group("N"))
            m = int(match.group("M"))
            c_nm = float(match.group("CNM").replace('D', 'E'))
            s_nm = float(match.group("SNM").replace('D', 'E'))

            if n >= PC_N_MAX:
                break

            snms[n, m] = s_nm
            cnms[n, m] = c_nm
            pass
        pass
    for n in range(0, PC_N_MAX):
        jns[n] = np.sqrt(cnms[n, 0] ** 2 + snms[n, 0] ** 2)
        pass

    return cnms, snms, jns


CNMS, SNMS, JNS = _get_perturbation_coefs(
    os.path.join(
        os.path.dirname(__file__),
        "jgm3.txt"
    )
)


def u_zonal(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E,
        js: list = JNS) -> float:

    j_subset = np.array(js[0:n_max + 1])
    r_ratios = np.array([(r_p / r) ** i for i in range(0, n_max + 1)])
    x = sind(phi)

    sumatory = legval(x, j_subset * r_ratios)
    return mu_p / r * sumatory


def p_nm(x: float, _n: int, _m: int):
    c = np.zeros(_n + 1)
    c[_n] = 1e0
    return (1e0 - x ** 2) ** (float(_m) / 2e0) * legval(x, legder(c, _m))


def u_sectorial(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E,
        cnms: np.ndarray = CNMS,
        snms: np.ndarray = SNMS) -> float:

    sumatory = 0e0
    for n in range(0, n_max):
        m = n

        t1 = (r_p / r) ** n
        t21 = cnms[n, n] * cosd(n * lamb)
        t22 = snms[n, n] * sind(n * lamb)
        t3 = p_nm(sind(phi), n, m)
        sumatory = sumatory + t1 * (t21 + t22) * t3
        pass
    return mu_p / r * sumatory


def u_teseral(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E,
        cnms: np.ndarray = CNMS,
        snms: np.ndarray = SNMS) -> float:

    sumatory = 0e0
    for n in range(0, n_max):
        for m in range(0, n):
            t1 = (r_p / r) ** n
            t21 = cnms[n, m] * cosd(m * lamb)
            t22 = snms[n, m] * sind(m * lamb)
            t3 = p_nm(sind(phi), n, m)
            sumatory = sumatory + t1 * (t21 + t22) * t3
            pass
        pass
    return mu_p / r * sumatory


def u_pert(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> float:

    uz = u_zonal(r=r, lamb=lamb, phi=phi, n_max=n_max, mu_p=mu_p, r_p=r_p)
    us = u_sectorial(r=r, lamb=lamb, phi=phi, n_max=n_max, mu_p=mu_p, r_p=r_p)
    ut = u_teseral(r=r, lamb=lamb, phi=phi, n_max=n_max, mu_p=mu_p, r_p=r_p)
    return uz + us + ut


def grad(f, x0: np.ndarray, dx: float = 1e-6):
    n = x0.size
    delta = np.zeros(n)
    g = np.zeros(n)

    for i in range(0, n):
        delta[i] = dx
        g[i] = (f(x0 + delta * dx / 2e0) - f(x0 - delta * dx / 2e0)) / dx
        pass

    return g


def grad_sph(f, x0: np.ndarray, dx: float = 1e-6):
    n = x0.size
    delta = np.zeros(n)
    g = np.zeros(n)

    for i in range(0, n):
        delta[i] = 1e0
        g[i] = (f(x0 + delta * dx / 2e0) - f(x0 - delta * dx / 2e0)) / dx
        delta[i] = 0e0
        pass

    g[1] = g[1] / (x0[0] * cosd(x0[2]))
    g[2] = g[2] / x0[0]

    return g


def _a_pert_sph(
        r: float,
        lamb: float,
        phi: float,
        n_max: int,
        mu_p: float,
        r_p: float,
        func
) -> np.ndarray:

    x0 = np.array([r, lamb, phi])

    def _f(x):
        return func(x[0], x[1], x[2], n_max, mu_p, r_p)

    return - grad_sph(_f, x0)


def a_pert_sph(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> np.ndarray:

    return _a_pert_sph(r, lamb, phi, n_max, mu_p, r_p, u_pert)


def a_zonal_sph(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> np.ndarray:

    return _a_pert_sph(r, lamb, phi, n_max, mu_p, r_p, u_zonal)


def a_sectorial_sph(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> np.ndarray:

    return _a_pert_sph(r, lamb, phi, n_max, mu_p, r_p, u_sectorial)


def a_teseral_sph(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> np.ndarray:

    return _a_pert_sph(r, lamb, phi, n_max, mu_p, r_p, u_teseral)


def a_pert(
        r: float,
        lamb: float,
        phi: float,
        n_max: int = N_MAX_DEF,
        mu_p: float = MU_E,
        r_p: float = R_E) -> np.ndarray:

    x0 = np.array(
        [
            r * cosd(phi) * cosd(lamb),
            r * cosd(phi) * sind(lamb),
            r * sind(phi)
        ]
    )

    def _f(x):
        return u_pert(x[0], x[1], x[2], n_max, mu_p, r_p)

    return - grad_sph(_f, x0)


__all__ = [
    "u_pert", "u_zonal", "u_sectorial", "u_teseral",
    "a_pert_sph", "a_pert",
    "a_sectorial_sph", "a_teseral_sph", "a_zonal_sph",
    "p_nm"]
