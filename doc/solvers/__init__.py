import numpy as np


def select_var(x, param):
    xx = x.copy()
    for field in xx.fields:
        v_x = getattr(x, field)
        v_p = getattr(param, field)
        not_param = v_p is None
        setattr(xx, field, v_x if not_param else v_p)
        pass
    return xx


def op2vec(op) -> np.ndarray:
    xx = list()
    for field in op.fields:
        val = getattr(op, field)
        if val is not None:
            xx.append(val)
            pass
        else:
            pass
        pass
    return np.array(xx)


def vec2op(vec: np.ndarray, param):
    op = param.copy()
    i = 0
    for field in op.fields:
        if getattr(param, field) is None:
            setattr(op, field, vec[i])
            i = i + 1
            pass
        else:
            setattr(op, field, None)
            pass
        pass
    return op


def jacobian(op0, f, param, dx: float = 1e-5) -> np.ndarray:
    x0 = op2vec(op0)

    n = len(x0)
    jac = np.zeros((n, n))
    delta_vec = np.zeros(n)

    for i in range(0, n):
        delta_vec[i] = dx
        jac[:, i] = (f(vec2op(x0 + delta_vec, param)) - f(op0)) / dx
        delta_vec[i] = 0e0
        pass

    return jac


def solver(
        parameters,
        initial_values,
        _functions: list,
        verbose: bool = False,
        output_values: list = None,
        dx: float = 1e-5,
        tol: float = 1e-6
):

    n_ecc = len(_functions)
    n_tot = len(parameters.fields)
    n_par = len(
        [f for f in parameters.fields if getattr(parameters, f) is not None]
    )

    gdl = n_tot - n_par - n_ecc

    if n_ecc + n_par != n_tot:
        fs = parameters.fields.copy()
        gdls = [p for p in fs if getattr(parameters, p) is None]
        print(
            "ERROR!, existen {0} grados de libertad:\n{1}".format(
                gdl, "\n".join(gdls)
            )
        )
        return None

    def _f(x):
        return np.array([ff(x, parameters) for ff in _functions])

    opf = initial_values.copy()
    for field in opf.fields:
        if getattr(parameters, field) is not None:
            setattr(opf, field, None)
            pass
        pass

    xf = op2vec(opf)
    delta_x = np.ones(len(xf)) * 1e3
    counter = 0

    while counter < 100 and np.linalg.norm(delta_x) > tol:
        a = jacobian(opf, _f, parameters,  dx=dx)
        b = -_f(opf)

        # print(a, '\n', delta_x, "\n")
        delta_x = np.linalg.solve(a=a, b=b)

        xf = xf + delta_x
        opf = vec2op(xf, parameters)

        counter = counter + 1
        pass

    if verbose or output_values is not None:
        opf = select_var(opf, parameters)
        if output_values is not None:
            for f in opf.fields:
                if f not in output_values:
                    setattr(opf, f, None)
                    pass
                pass
            pass
        pass
    else:
        pass

    print("### SOLVER SOLUTIONS ###")
    if counter >= 100:
        print("MAX ITERATIONS REACHED: {0}".format(counter))
        pass
    print("Max error: {}".format(np.max(np.abs(delta_x))))
    print("RMS: {}".format(np.linalg.norm(delta_x) / np.sqrt(delta_x.size)))
    print("Results:\n{}\n".format(opf))
    return opf
    pass


__all__ = ["solver", "select_var"]
