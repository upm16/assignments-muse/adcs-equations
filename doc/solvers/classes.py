from typing import Optional
from doc.constants import MU_E, R_E, pi
Param = Optional[float]

OP_DEFAULT_DICT = {
    "sma": (1e4, "km"),
    "ecc": (0e0, "-"),
    "inc": (0e0, "deg"),
    "raan": (0e0, "deg"),
    "arp": (0e0, "deg"),
    "ta": (0e0, "deg"),
    "mu_p": (MU_E, "km^3/s^2"),
    "r_p": (R_E, "km"),
    "r": (1e4, "km"),
    "rp": (1e4, "km"),
    "ra": (1e4, "km"),
    "hp": (3.6e3, "km"),
    "ha": (3.6e3, "km"),
    "period": (1e5, "s"),
    "v": (9e0, "km/s"),
    "vp": (9e0, "km/s"),
    "va": (9e0, "km/s"),
}

OP_FIELDS = list(OP_DEFAULT_DICT.keys())
OP_DEFAULT_TUPLES = list(OP_DEFAULT_DICT.values())
OP_DEFAULT_VALUES, OP_UNITS = (list(d) for d in zip(*OP_DEFAULT_TUPLES))
OP_UNITS_DICT = dict(zip(OP_FIELDS, OP_UNITS))


class OpBase:
    fields = []
    units = {}

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.fields:
                setattr(self, key, value)
                pass
            pass
        pass

    def __str__(self):
        text = ""
        patt = "{0}{1: <8}: {2: >16.5e} [{3}]\n"
        patt_none = "{0}{1: <8}: {2: >16} [{3}]\n"
        for field in self.fields:
            value = getattr(self, field)
            unit = self.units[field]
            if value is None:
                text = patt_none.format(text, field, " ", unit)
                pass
            else:
                text = patt.format(text, field, value, unit)

                if unit == "s":
                    text = patt.format(text, "|-->", value / 60., "min")
                    text = patt.format(text, "|---->", value / 3600., "h")
                    pass
                elif unit == "deg":
                    text = patt.format(text, "|-->", value * pi / 180., "rad")
            pass
        return text
        pass

    def copy(self):
        op2 = OpBase()
        for field in self.fields:
            setattr(op2, field, getattr(self, field))
            pass
        return op2

    pass


for _field in OP_FIELDS:
    setattr(OpBase, _field, None)
    pass


class OP(OpBase):
    fields = OP_FIELDS.copy()
    units = OP_UNITS_DICT.copy()

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in OP_FIELDS:
                setattr(self, key, value)
                pass
            pass
        pass

    def __str__(self):
        text = ""
        patt = "{0}{1: <6}: {2: >16.5f} [{3}]\n"
        patt_none = "{0}{1: <6}: {2: >16} [{3}]\n"
        for field in OP_FIELDS:
            value = getattr(self, field)
            unit = OP_UNITS_DICT[field]
            if value is None:
                text = patt_none.format(text, field, " ", unit)
                pass
            else:
                text = patt.format(text, field, value, unit)

                if field == "period":
                    text = patt.format(text, "|-->", value / 60., "min")
                    text = patt.format(text, "|---->", value / 3600., "h")
            pass
        return text
        pass

    def copy(self):
        op2 = OP()
        for field in self.fields:
            setattr(op2, field, getattr(self, field))
            pass
        return op2

    pass


OP_DEFAULT = OP(**dict(zip(OP_FIELDS, OP_DEFAULT_VALUES)))


class OpTerrestre(OP):
    pass


setattr(OpTerrestre, "mu_p", MU_E)
setattr(OpTerrestre, "r_p", R_E)



