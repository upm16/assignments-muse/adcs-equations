from .functions import triad, quest, q_method

__all__ = ["triad", "quest", "q_method"]
