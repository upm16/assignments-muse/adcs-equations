import numpy as np
from doc.kinematics.functions import q2cbi


def triad(ui, vi, ub, vb):
    t1_b = ub
    t2_b = np.cross(ub, vb) / np.linalg.norm(np.cross(ub, vb))
    t3_b = np.cross(t1_b, t2_b)

    t1_i = ui
    t2_i = np.cross(ui, vi) / np.linalg.norm(np.cross(ui, vi))
    t3_i = np.cross(t1_i, t2_i)

    ctb = np.array(
        [
            t1_b,
            t2_b,
            t3_b
        ]
    ).T

    cti = np.array(
        [
            t1_i,
            t2_i,
            t3_i
        ]
    ).T

    return np.matmul(ctb, cti.T)


def _q_parameters(vbs, vis, ws):
    n = len(vbs)
    b: np.ndarray = sum(ws[i] * np.outer(vbs[i], vis[i]) for i in range(0, n))
    k22 = np.trace(b)
    k11 = b + b.T - k22 * np.eye(3, 3)
    k12 = np.array(
        [
            b[1, 2] - b[2, 1], b[2, 0] - b[0, 2], b[0, 1] - b[1, 0]
        ]
    )
    k = np.zeros((4, 4))
    k[0:3, 0:3] = k11
    k[0:3, 3] = k12
    k[3, 0:3] = k12
    k[3, 3] = k22
    s = b + b.T

    return {"b": b, "k": k, "s": s, "k12": k12, "k22": k22, "k11": k11}


def quest(vbs: list, vis: list, ws: list) -> np.ndarray:

    n = len(ws)
    lamb0 = sum(ws)

    params = _q_parameters(vbs, vis, ws)
    k22 = params["k22"]
    k12 = params["k12"]
    s = params["s"]

    def cbi_from_lambda(lamb):
        _a = (lamb + k22) * np.identity(3) - s
        _p = np.linalg.solve(a=_a, b=k12)

        _qt = np.array([*list(_p), 1e0]) / np.linalg.norm(
            np.array([*list(_p), 1e0]))
        _q, _q4 = _qt[0:3], _qt[3]

        _c = q2cbi(_qt)
        return _c

    def newton(f, x0, dx=1e-6, imax=500, err=1e-7):
        x1, x2 = 1e10, x0
        it, _ITER_MAX = 0, imax
        while abs(x2 - x1) > err and it <= _ITER_MAX:
            x1 = x2
            fp = (f(x1 + dx) - f(x1 - dx)) / (2e0 * dx)
            x2 = x1 - f(x1) / fp
            it = it + 1
            pass
        if it > _ITER_MAX:
            print("Error, Maximum iteration reached.")
        return x2

    def f_zero(lamb):
        exp1 = lamb

        c_bi_actual = cbi_from_lambda(lamb)
        exp2 = sum(
            [
                ws[i] * np.dot(vbs[i], np.matmul(c_bi_actual, vis[i]))
                for i in range(0, n)
            ]
        )
        return exp1 - exp2

    lambda_max = newton(f_zero, lamb0)

    mat = cbi_from_lambda(lambda_max)
    return mat


def q_method(vbs: list, vis: list, ws: list) -> np.ndarray:
    params = _q_parameters(vbs, vis, ws)
    k = params["k"]

    evals, evecs = np.linalg.eig(k)
    idx = np.argmax(evals)
    lamb, q = evals[idx], evecs[:, idx]

    return q2cbi(q)


__all__ = ["triad", "quest", "q_method"]
