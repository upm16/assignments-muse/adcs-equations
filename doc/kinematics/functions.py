import numpy as np
from typing import Union, Optional
from doc.constants import cosd, sind
from math import acos, pi

Param = Union[np.ndarray, list]


def c_ab(
        a1: Param,
        a2: Param,
        a3: Param,
        b1: Param,
        b2: Param,
        b3: Param) -> np.ndarray:
    """Get transformation matrix from system 'B' to system 'A'.

    :param: a1: vector 1 on system 'A'.
    :param: a2: vector 2 on system 'A'.
    :param: a3: vector 3 on system 'A'.
    :param: b1: vector 1 on system 'B'.
    :param: b2: vector 2 on system 'B'.
    :param: b3: vector 3 on system 'B'.
    """

    c_ai = np.zeros((3, 3))
    c_bi = np.zeros((3, 3))

    c_ai[0, :] = np.array(a1)
    c_ai[1, :] = np.array(a2)
    c_ai[2, :] = np.array(a3)

    c_bi[0, :] = np.array(b1)
    c_bi[1, :] = np.array(b2)
    c_bi[2, :] = np.array(b3)

    c = np.matmul(c_ai, np.transpose(c_bi))

    del c_ai
    del c_bi

    return c


def skew(w: Param) -> np.ndarray:
    """Skew-symetric matrix
    :param: w: vector.
    """

    c = np.zeros((3, 3))
    c[0, 1] = -w[2]
    c[0, 2] = w[1]
    c[1, 2] = -w[0]
    c = c - np.transpose(c)
    return c


def trsf_z(theta_deg: float) -> np.ndarray:
    return np.array(
        [
            [cosd(theta_deg), sind(theta_deg), 0e0],
            [-sind(theta_deg), cosd(theta_deg), 0e0],
            [0e0, 0e0, 1e0]
        ]
    )


def trsf_y(theta_deg: float) -> np.ndarray:
    return np.array(
        [
            [cosd(theta_deg), 0e0, -sind(theta_deg)],
            [0e0, 1e0, 0e0, ],
            [sind(theta_deg), 0e0, cosd(theta_deg)],
        ]
    )


def trsf_x(theta_deg: float) -> np.ndarray:
    return np.array(
        [
            [1e0, 0e0, 0e0],
            [0e0, cosd(theta_deg), sind(theta_deg)],
            [0e0, -sind(theta_deg), cosd(theta_deg)]

        ]
    )


def trsf_313(t1_deg: float, t2_deg: float, t3_deg: float) -> np.ndarray:
    """Precesion (t1), Rotacion (t2) y Nutacion (t3):
    C_bi = C_z(t3) * C_x(t2) * C_z(t1)

    :param: t1_deg: Precesion (primer giro).
    :param: t2_deg: Rotacion (segundo giro).
    :param: t3_deg: Nutacion (tercer giro).

    """

    return np.matmul(trsf_z(t3_deg), np.matmul(trsf_x(t2_deg), trsf_z(t1_deg)))


def trsf_321(t1_deg: float, t2_deg: float, t3_deg: float) -> np.ndarray:
    """Guinada (t1), Cabeceo (t2) y Alabeo (t3):
    C_bi = C_x(t3) * C_y(t2) * C_z(t1)

    :param: t1_deg: Guinada (primer giro).
    :param: t2_deg: Cabeceo (segundo giro).
    :param: t3_deg: Alabeo (tercer giro).

    """

    return np.matmul(trsf_x(t3_deg), np.matmul(trsf_y(t2_deg), trsf_z(t1_deg)))


def trsf_sec_generic(
        sec: list,
        ts_deg: list) -> Optional[np.ndarray]:
    """Any angle transformation matrix:
    C_bi = ... * C_{sec_3}(t3) * C_{sec_2}(t2) * C_{sec_1}(t1).

    :param: sec: secuencia de angulos, p.e. [313], [2323], [13121], ...
    :param: ts_deg: angulos de giro en orden de rotacion.
    """

    if len(sec) == len(ts_deg) and all([0 < x <= 3 for x in sec]):
        c = np.eye(3, 3)
        for s, t in zip(sec, ts_deg):
            if s == 1:
                c = np.matmul(trsf_x(t), c)
            elif s == 2:
                c = np.matmul(trsf_y(t), c)
            elif s == 3:
                c = np.matmul(trsf_z(t), c)
                pass
            pass
        return c
    else:
        print(
            "ERROR! la secuencia no es de longitud 3 o\
            la secuencia no esta entr 1 y 3"
        )
        return None
    pass


def trsf_sec(
        sec: list,
        t1_deg: float,
        t2_deg: float,
        t3_deg: float) -> Optional[np.ndarray]:
    """Any 3-angle transformation matrix:
    C_bi = C_{sec_3}(t3) * C_{sec_2}(t2) * C_{sec_1}(t1)
    :param: sec: secuencia de angulos, p.e. [313], [323], [121], ...
    :param: t1_deg: primer giro.
    :param: t2_deg: segundo giro.
    :param: t3_deg: tercer giro.
    """

    if len(sec) == 3 and all([0 < x <= 3 for x in sec]):
        angles = [t1_deg, t2_deg, t3_deg]
        return trsf_sec_generic(sec, angles)
    else:
        print(
            "ERROR! la secuencia no es de longitud 3 o\
            la secuencia no esta entr 1 y 3"
        )
        return None
    pass


def eig2cbi(eigenaxis: Param, phi_deg: float) -> np.ndarray:
    e = np.array(eigenaxis)
    t1 = cosd(phi_deg) * np.eye(3, 3)
    t2 = (1e0 - cosd(phi_deg)) * np.outer(e, e)
    t3 = - sind(phi_deg) * skew(e)
    return t1 + t2 + t3


def cbi2eig(c_bi: np.ndarray) -> tuple:
    phi_deg = 180. / pi * acos(5e-1 * (np.trace(c_bi) - 1e0))
    eig = np.array(
        [
            c_bi[1, 2] - c_bi[2, 1],
            c_bi[2, 0] - c_bi[0, 2],
            c_bi[0, 1] - c_bi[1, 0],
        ]
    ) / (2e0 * sind(phi_deg))
    return eig, phi_deg


def eig2q(eigenaxis: Param, phi_deg: float) -> np.ndarray:
    e = np.array(eigenaxis)
    q = np.zeros(4)
    q[0:3] = e * sind(phi_deg / 2e0)
    q[3] = cosd(phi_deg / 2e0)
    return q


def q2cbi(q: Param) -> np.ndarray:
    qq = np.array(q)
    qv = qq[0:3]
    q4 = qq[3]

    t1 = (q4 ** 2 - np.dot(qv, qv)) * np.eye(3, 3)
    t2 = 2e0 * np.outer(qv, qv)
    t3 = -2e0 * q4 * skew(qv)

    return t1 + t2 + t3


def cbi2q(c_bi: np.ndarray) -> np.ndarray:
    e, p = cbi2eig(c_bi)
    return eig2q(e, p)


def q2eig(q: Param) -> tuple:
    c_bi = q2cbi(q)
    return cbi2eig(c_bi)


def kin_cbi(omega: Param, c_bi: np.ndarray) -> np.ndarray:
    w = np.array(omega)
    return - np.matmul(skew(w), c_bi)


def kin_sec(
        omega: Param,
        sec: list,
        t1_deg: float,
        t2_deg: float,
        t3_deg: float) -> np.ndarray:

    w = np.array(omega)
    angles = [t1_deg, t2_deg, t3_deg]
    c = np.zeros((3, 3))

    for i in range(0, 3):
        s = sec[i]
        c_actual = trsf_sec_generic(sec[i:], angles[i:])
        u = c_actual[:, s - 1]
        c[:, i] = u
        pass
    return np.linalg.solve(a=c, b=w) * 180. / pi
    pass


def kin_q(omega: Param, q: Param) -> np.ndarray:
    w = np.array(omega)
    qq = np.array(q)

    big_omega = np.zeros((4, 4))
    big_omega[0:3, 0:3] = -skew(w)
    big_omega[0:3, 3] = w
    big_omega[3, 0:3] = -w

    return 5e-1 * np.matmul(big_omega, qq)


def kin_eig(omega: Param, eigenaxis: Param, phi_deg: float) -> tuple:
    w = np.array(omega)
    e = np.array(eigenaxis)
    phi_rad = phi_deg * pi / 180.

    d_phi_deg = 180. / pi * np.outer(e, w)

    a = skew(e) - np.matmul(skew(e), skew(e)) / np.tan(phi_rad / 2e0)
    d_eig = 5e-1 * np.matmul(a, w)
    return d_eig, d_phi_deg
