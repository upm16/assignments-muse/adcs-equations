from doc.solvers import solver
from .classes import C_DEFAULT
from .classes import Control
from .functions import C_FUNCS


def solver_control(
        parameters: Control,
        initial_values: Control = C_DEFAULT,
        _functions: list = C_FUNCS,
        verbose: bool = False,
        results=None
):

    return solver(
        parameters=parameters,
        initial_values=initial_values,
        _functions=_functions,
        verbose=verbose,
        output_values=results
    )


__all__ = ["solver_control", "Control"]
