from doc.solvers.classes import OpBase

C_DICT = {
    "w_n": "rad/s",
    "w_d": "rad/s",
    "beta": "rad",
    "damp": "-",
    "m_p": "%",
    "t_s": "s",
    "t_p": "s",
    "t_r": "s",
    "s_real": "-",
    "s_imag": "-",
}

C_FIELDS = list(C_DICT.keys())


class Control(OpBase):
    fields = C_FIELDS.copy()
    units = C_DICT.copy()

    def copy(self):
        op2 = Control()
        for field in self.fields:
            setattr(op2, field, getattr(self, field))
            pass
        return op2

    pass


setattr(Control, "w_n", None)
setattr(Control, "damp", None)
setattr(Control, "beta", None)
setattr(Control, "w_d", None)
setattr(Control, "m_p", None)
setattr(Control, "t_s", None)
setattr(Control, "t_r", None)
setattr(Control, "t_p", None)
setattr(Control, "s_real", None)
setattr(Control, "s_imag", None)

C_DEFAULT = Control(
    w_n=1e-4,
    damp=2e-1,
    beta=1e0,
    w_d=1e-4,
    m_p=20,
    t_s=60,
    t_r=20,
    t_p=30,
    s_real=1e-2,
    s_imag=1e-3
)
