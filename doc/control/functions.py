from doc.solvers import select_var
from math import sqrt, pi, atan, exp, log


def damped_natural_freq(w_n: float, damp: float):
    return w_n * sqrt(1e0 - damp ** 2)


def beta(damp: float):
    return atan(sqrt(1e0 - damp ** 2) / damp)


def rise_time(w_n: float, damp: float):
    return (pi - beta(damp)) / damped_natural_freq(w_n, damp)


def peak_time(w_n: float, damp: float):
    return pi / damped_natural_freq(w_n, damp)


def overshoot(damp: float):
    return 1e2 * exp(-pi * damp / sqrt(1e0 - damp ** 2))


def setting_time(w_n: float, damp: float):
    return 4.4 / damp / w_n
    # return - log(0.02 * sqrt(1e0 - damp ** 2)) / (damp * w_n)


def setting_time_real(w_n: float, damp: float):
    return - log(0.02 * sqrt(1e0 - damp ** 2)) / (damp * w_n)


def system_s_real(w_n: float, damp: float):
    return w_n * damp


def system_s_imag(w_n: float, damp: float):
    return w_n * sqrt(1e0 - damp ** 2)


# Zero Funcs
def _f_wd(x, param):
    xx = select_var(x, param)
    t1 = xx.w_d
    t2 = damped_natural_freq(xx.w_n, xx.damp)
    return t1 - t2


def _f_beta(x, param):
    xx = select_var(x, param)
    t1 = xx.beta
    t2 = beta(xx.damp)
    return t1 - t2


def _f_tr(x, param):
    xx = select_var(x, param)
    t1 = xx.t_r
    t2 = rise_time(xx.w_n, xx.damp)
    return t1 - t2


def _f_tp(x, param):
    xx = select_var(x, param)
    t1 = xx.t_p
    t2 = peak_time(xx.w_n, xx.damp)
    return t1 - t2


def _f_mp(x, param):
    xx = select_var(x, param)
    t1 = xx.m_p
    t2 = overshoot(xx.damp)
    return t1 - t2


def _f_ts(x, param):
    xx = select_var(x, param)
    return xx.t_s - setting_time(xx.w_n, xx.damp)


def _f_sr(x, param):
    xx = select_var(x, param)
    return xx.s_real - system_s_real(xx.w_n, xx.damp)


def _f_si(x, param):
    xx = select_var(x, param)
    return xx.s_imag - system_s_imag(xx.w_n, xx.damp)


C_FUNCS = [
    _f_wd, _f_beta,
    _f_mp, _f_tp, _f_tr, _f_ts,
    _f_si, _f_sr
]

__all__ = [
    "damped_natural_freq", "beta", "overshoot",
    "peak_time", "rise_time", "setting_time", "setting_time_real",
    "system_s_real", "system_s_imag",
    "C_FUNCS"
]
