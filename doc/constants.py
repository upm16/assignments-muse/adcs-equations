from math import cos, sin, pi

H: float = 6.626e-34  # Constate de Plank [J s]
C: float = 3e8  # Velocidad de la luz [m/s]
K: float = 1.38e-23  # Constante de Boltzman
L: float = 3.9e26  # Luminosidad Solar [W]
PHI: float = 1362  # Constante Solar [W m^-2]
SIGMA: float = 5.67e-8  # Constante Stefan Boltzman [W m^-2 K^-4]
MU_E: float = 3.986e5  # km^3/s^2
R_E: float = 6378e0  # km
J2: float = 0.001082  # Earth bulge perturbation [-]
J3: float = 0.2532435346E-05  # J_3 [-]
# J3: float =
OMEGA_SUN: float = 2e0 * pi / (365.25 * 24. * 3600.)  # [rad/s]
UA: float = 149597900.  # 1 Astronomical Unit [km]


def cosd(ang: float) -> float:
    return cos(ang * pi / 180.)


def sind(ang: float) -> float:
    return sin(ang * pi / 180.)
