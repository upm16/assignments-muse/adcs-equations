from ..solvers.classes import OpBase


class OpOrbitClass(OpBase):
    fields = ["raan_sec", "arp_sec", "sma", "inc", "ecc"]
    units = {
        "raan_sec": "rad/s",
        "arp_sec": "rad/s",
        "sma": "km",
        "inc": "deg",
        "ecc": "-"
    }

    def copy(self):
        op2 = OpOrbitClass()
        for field in self.fields:
            setattr(op2, field, getattr(self, field))
            pass
        return op2

    pass


setattr(OpOrbitClass, "raan_sec", None)
setattr(OpOrbitClass, "arp_sec", None)
setattr(OpOrbitClass, "sma", None)
setattr(OpOrbitClass, "ecc", None)
setattr(OpOrbitClass, "inc", None)

OB_DEFAULT = OpOrbitClass(
    raan_sec=1e-8,
    arp_sec=1e-8,
    sma=1e5,
    ecc=5e-1,
    inc=100e0)


