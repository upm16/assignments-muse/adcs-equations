from ..solvers import solver
from .classes import OpOrbitClass, OB_DEFAULT
from .functions import ORB_FUNCS


def solver_secular(
        parameters: OpOrbitClass,
        initial_values: OpOrbitClass = OB_DEFAULT,
        _functions: list = ORB_FUNCS,
        verbose: bool = False,
        results=None,
        dx=1e-9,
        tol=1e-8
):

    return solver(
        parameters=parameters,
        initial_values=initial_values,
        _functions=_functions,
        verbose=verbose,
        output_values=results,
        dx=dx,
        tol=tol
    )


__all__ = ["solver_secular", "OpOrbitClass"]
