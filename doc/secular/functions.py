from doc.constants import MU_E, R_E, J2, cosd
from ..solvers import select_var
from math import sqrt, pi


def raan_sec(sma: float, ecc: float, inc: float) -> float:
    t1: float = 3e0 * J2 * R_E ** 2 / 2e0 / (1e0 - ecc ** 2) ** 2
    t2: float = sqrt(MU_E / sma ** 7)
    t3: float = cosd(inc)
    return - t1 * t2 * t3


def arp_sec(sma: float, ecc: float, inc: float) -> float:
    t1: float = 3e0 * pi * J2 * R_E ** 2 / 4e0 / (1e0 - ecc ** 2) ** 2
    t2: float = sqrt(MU_E / sma ** 7)
    t3: float = 5e0 * cosd(inc) ** 2 - 1e0
    return t1 * t2 * t3
    pass


def _f_raan_sec(x, param):
    xx = select_var(x, param)
    return xx.raan_sec - raan_sec(xx.sma, xx.ecc, xx.inc)


def _f_arp_sec(x, param):
    xx = select_var(x, param)
    return xx.arp_sec - arp_sec(xx.sma, xx.ecc, xx.inc)


ORB_FUNCS = [_f_arp_sec, _f_raan_sec]


__all__ = ["raan_sec", "arp_sec", "ORB_FUNCS"]
