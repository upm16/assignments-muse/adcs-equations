from .classes import OpTerrestre2D, OpTerrestre
from doc.constants import cosd
from ..solvers import select_var
from math import sqrt, pi
from typing import Union


Param = Union[OpTerrestre, OpTerrestre2D]


# Auxiliary functions
def radii(sma, ecc, ta):
    return sma * (1e0 - ecc ** 2) / (1e0 + ecc * cosd(ta))


def perigee_radii(sma, ecc):
    return sma * (1e0 - ecc)


def apogee_radii(sma, ecc):
    return sma * (1e0 + ecc)


def perigee_altitude(sma, ecc, r_p):
    return perigee_radii(sma, ecc) - r_p


def apogee_altitude(sma, ecc, r_p):
    return apogee_radii(sma, ecc) - r_p


def orbit_period(sma, mu_p):
    return 2e0 * pi * sqrt(sma ** 3 / mu_p)


def velocity(sma, r, mu_p):
    return sqrt(2e0 * mu_p * (1e0 / r - 1e0 / 2e0 / sma))


def perigee_velocity(sma, ecc, mu_p):
    return velocity(sma, perigee_radii(sma, ecc), mu_p)


def apogee_velocity(sma, ecc, mu_p):
    return velocity(sma, apogee_radii(sma, ecc), mu_p)


# Functions
def _f_r(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    t1: float = xx.r
    t2: float = radii(xx.sma, xx.ecc, xx.ta)

    return t1 - t2


def _f_rp(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    t1: float = xx.rp
    t2: float = perigee_radii(xx.sma, xx.ecc)
    return t1 - t2


def _f_ra(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.ra - apogee_radii(xx.sma, xx.ecc)


def _f_hp(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.hp - perigee_altitude(xx.sma, xx.ecc, xx.r_p)


def _f_ha(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.ha - apogee_altitude(xx.sma, xx.ecc, xx.r_p)


def _f_period(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.period - orbit_period(xx.sma, xx.mu_p)


def _f_v(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.v - velocity(xx.sma, xx.r, xx.mu_p)


def _f_vp(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.vp - perigee_velocity(xx.sma, xx.ecc, xx.mu_p)


def _f_va(x: Param, param: Param) -> float:
    xx = select_var(x, param)
    return xx.va - apogee_velocity(xx.sma, xx.ecc, xx.mu_p)


OP_FUNCS = [
    _f_r, _f_rp, _f_ra, _f_hp, _f_ha, _f_period, _f_v, _f_va, _f_vp
]
