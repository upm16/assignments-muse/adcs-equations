from ..solvers import solver
from ..solvers.classes import OP_DEFAULT
from .classes import OpTerrestre2D
from .functions import OP_FUNCS


def solver_orbit2d(
        parameters: OpTerrestre2D,
        initial_values: OpTerrestre2D = OP_DEFAULT,
        _functions: list = OP_FUNCS,
        verbose: bool = False,
        results=None
):

    return solver(
        parameters=parameters,
        initial_values=initial_values,
        _functions=_functions,
        verbose=verbose,
        output_values=results
    )


__all__ = ["solver_orbit2d", "OpTerrestre2D"]
