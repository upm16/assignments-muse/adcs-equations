from doc.doc2.orbital2d import OpTerrestre2D as Op, solver_orbit2d as solver
from doc.doc2.secular import solver_orbit_types, OpOrbitClass as Ob
from doc.constants import OMEGA_SUN
from doc import attitude
from math import sin, cos
import numpy as np
import doc
import matplotlib.pyplot as plt


if __name__ == "__main__":
    params = Op()
    params.ta = 0e0
    params.hp = 300e0
    params.ha = 10e3
    solver(params)

    params = Ob()
    params.sma = 10e3
    params.raan_sec = OMEGA_SUN
    params.arp_sec = 0e0

    sol = solver_orbit_types(params)

    # print(
    #     attitude.trsf_sec(
    #         sec=[3, 2, 1],
    #         t1_deg=87e0,
    #         t2_deg=-120e0,
    #         t3_deg=45e0
    #     ) - attitude.trsf_321(87e0, -120e0, 45e0)
    # )

    ws = [1e0, 1e0]
    ne_b = np.array([-0.70710678, 0.70710678, 0.])
    ns_b = np.array([0.70710678, 0.70710678, 0.])

    ne_i = np.array([-1, 0, 0], dtype=float)
    ns_i = np.array([0, 1, 0], dtype=float)

    vbs = [ne_b, ns_b]
    vis = [ne_i, ns_i]

    print(doc.triad.quest(vbs, vis, ws))
    print(doc.triad.q_method(vbs, vis, ws))

    def f(x, t):
        omega_list = [sin(0.1 * t), 0.01, cos(0.1 * t)]
        omega = np.array(omega_list) * 5e0
        omega = omega * np.pi / 180.

        return doc.attitude.kin_sec(
            omega=omega,
            sec=[3, 2, 1],
            t1_deg=x[0],
            t2_deg=x[1],
            t3_deg=x[2]
        )

    t0 = 0e0
    x0 = [80e0, 30e0, 40e0]
    x0.reverse()
    dt = 1e-2
    t = t0
    tf = 60e0
    n_max = 100
    ts = np.linspace(0e0, tf, n_max)
    xs = np.zeros((n_max, 3))
    i = 0
    x_next = x0
    while t <= tf:
        x_next = x_next + dt * f(x_next, t)
        if t >= ts[i]:
            xs[i, :] = x_next
            i = i + 1
            pass
        t = t + dt
        pass

    plt.plot(ts, xs[:, 2])
    #plt.xlim([0e0, 60e0])
    #plt.ylim([50e0, 200e0])
    plt.grid(True)
    #plt.xticks(range(0, 60, 10))
    #plt.yticks(range(50, 200, 50))

    axs = plt.gca()
    axs.set_aspect(0.25)

    #plt.plot(ts, xs[:, 1])
    #plt.plot(ts, xs[:, 2])
    plt.legend(["T1", "T2", "T3"])
    plt.show()


    def f(q, t):
        omega_list = [sin(0.1 * t), 0.01, cos(0.1 * t)]
        omega = np.array(omega_list) * 50e0 * np.pi / 180.

        return doc.attitude.kin_q(omega=omega, q=q)

    t0 = 0e0
    q0 = [0.51563, 0.398665, 0.0967425, 0.752219]
    dt = 1e-2
    t = t0
    tf = 60e0
    n_max = 100
    ts = np.linspace(0e0, tf, n_max)
    xs = np.zeros((n_max, 4))
    i = 0
    x_next = q0
    while t <= tf:
        x_next = x_next + dt * f(x_next, t)
        if t >= ts[i]:
            xs[i, :] = x_next
            i = i + 1
            pass
        t = t + dt
        pass

    f = plt.subplots(3, 2)
    plt.subplot(2,2,1)
    plt.plot(ts, xs[:, 0])
    plt.subplot(2, 2, 2)
    plt.plot(ts, xs[:, 1])
    plt.subplot(2, 2, 3)
    plt.plot(ts, xs[:, 2])
    plt.subplot(2, 2, 4)
    plt.plot(ts, xs[:, 3])
    plt.xlim([0e0, 60e0])
    plt.ylim([-1e0, 1e0])
    plt.grid(True)
    #plt.xticks(range(0, 60, 10))
    #plt.yticks(range(50, 200, 50))

    #axs = plt.gca()
    #axs.set_aspect(1)

    #plt.plot(ts, xs[:, 1])
    #plt.plot(ts, xs[:, 2])
    plt.legend(["q1", "q2", "q3", "q4"])
    plt.show()

    plt.semilogy(np.linalg.norm(xs[:-1,:], axis=1))
    plt.show()


    pass